enablePlugins(JavaAppPackaging, DockerPlugin)

packageName in Docker          := "playwebservice"

version in Docker              := "1.0"

dockerBaseImage                := "dockerfile/java:oracle-java7"

dockerRepository               := Some("nxgened")

dockerExposedPorts in Docker   := Seq(9000)

dockerExposedVolumes in Docker := Seq("/opt/docker/logs")

dockerUpdateLatest             := true

daemonUser in Docker           := "root"

name                           := "playwebservice"

version                        := "1.0"

bashScriptConfigLocation       := Some("/opt/docker/conf/etc-default")

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.10.4"

libraryDependencies ++= Seq(
  jdbc,
  anorm,
  cache,
  ws,
  "postgresql" % "postgresql" % "9.1-901-1.jdbc4",
  "commons-io" % "commons-io" % "2.4",
  "commons-validator" % "commons-validator" % "1.4.0"
)

