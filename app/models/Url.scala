package models

import anorm._
import play.api.db.DB
import play.api.Play.current
import play.api.Logger

/**
 * Created by german on 17/12/14.
 */
/**
 * Created by german on 16/12/14.
 */
case class Url(url: String)

object UrlModel {

  /**
   * add url
   * @param url
   * @return
   */
  def create(url: String) = {

    //Log
    Logger.info("Model - Create url")

    DB.withConnection { implicit connection =>
      val result: Option[Long] = SQL("INSERT INTO Urls(url) VALUES   ({url})").on(
           'url  -> url
      ).executeInsert()

      //TODO log the id added
      //val message = result.toString
      //Logger.debug(message)

      result
    }
  }

  def delete(url: String) = {
    //Log
    Logger.info("Model - Delete url")

    DB.withConnection { implicit connection =>
      val result: Int = SQL("DELETE FROM Urls WHERE url = ({url})").on('url -> url).executeUpdate()
      result
    }
  }

  def get_urls = {
    //Log
    Logger.info("Model - Get Urls.")

    DB.withConnection { implicit connection =>

      val result = SQL("SELECT * FROM Urls")

      // Transform the resulting Stream[Row] as a List[(String)]
      val urlsList = result().map(row =>
        row[String]("url")
      ).toList
      urlsList
    }
  }
}
