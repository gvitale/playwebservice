package controllers

import org.apache.commons.codec.binary.Base64
import org.apache.commons.io.IOUtils
import play.api.libs.json.{JsValue, Json}
import play.api.mvc._
import models.UrlModel
import play.api.data._
import play.api.data.Forms._
import play.api.data.validation.Constraints
import java.util.Date
import models.Url
import org.apache.commons.validator.routines.UrlValidator

import play.api.Logger
import scala.concurrent.Future

//import play.api._
//import controllers.WebService//TODO delete this

object Application extends Controller{

  val version: Double = 1.0
  val status = "status"
  val reason = "reason"
  val ok = "OK"
  val error = "Error"
  val sites = "Sites"

  def getversion = Action {
    //Log
    Logger.info("Action - Get Version.")
    Ok(versionJson(version))
  }

  def index(url: String) = Action {
    //Log
    Logger.info("Action - Index.")

    models.UrlModel.create(url)

    Ok(views.html.index(url))
  }

  def addurl(url: String) = Action{
    //Log
    Logger.info("Action - Add Url.")

    //check if string is not empty
    if(!url.isEmpty){
      //Check if url format is correct
      val schemes: Array[String] = Array("http", "https")
      val urlValidator = new UrlValidator(schemes)
      if (urlValidator.isValid(url)) {//correct format
        val result = UrlModel.create(url)
        result match {
          case _: Some[Long] => Ok(statusOk)
          case _ => Ok(statusError("algun error..."))//TODO get error
        }
      } else {
        Logger.error("Url format is not valid.")
        Ok(statusError("Url format is not valid"))//TODO check
      }
    }else{
      Logger.error("The url String is empty.")
      Ok(statusError("The url String is empty"))//TODO check
    }
  }

  def deleteurl(url: String) = Action {
    //Log
    Logger.info("Action - Delete Url.")

    val result = UrlModel.delete(url)

    result match {
      case 1 => Ok(statusOk)
      case 0 => Ok(statusError("There are not URL with that name in Database"))//TODO get error
      case _ => Ok(statusError("Database Conection failed"))//TODO get error
    }
  }

  def urls = Action {
    //Log
    Logger.info("Action - Get Urls.")

    val AllUrls = UrlModel.get_urls

    Ok(allSites(AllUrls))

  }

  /*def urls_browser = Action {
    //Log
    Logger.info("Action - Get Urls.")

    val AllUrls = UrlModel.get_urls

    //Transform the resulting List[Stirng] as a Json
    // val jsonAllUrls = Json.toJson(AllUrls)
    //jsonAllUrls
    Ok(views.html.urls(AllUrls))

  }*/

  def decodeBase64(content : String) = {
    val decoded = Base64.decodeBase64(content)
    val finalString = IOUtils.toString(decoded)//TODO
    finalString
  }

  def statusOk = {
    val json: JsValue = Json.obj(
      status -> ok
    )

    json
  }

  def statusError(errorReason: String) = {
    val json: JsValue = Json.obj(
      status -> error,
      reason -> errorReason
    )

    json
  }

  def allSites(urls: List[String]) = {
    val json: JsValue = Json.obj(
      sites -> Json.toJson(urls)
    )

    json
  }

  def versionJson(version: Double) = {
    val json: JsValue = Json.obj(
      "version" -> version
    )

    json
  }
}


