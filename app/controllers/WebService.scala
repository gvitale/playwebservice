package controllers

import anorm.Sql
import play.api._
import play.api.mvc._
import scala.io.Source

/**
 * Created by german on 17/12/14.
 */
object WebService extends Controller{
  val version = 1.0

  def get_version = {
    version
  }

  def get_urls = {
    val urls = models.UrlModel.get_urls
    urls
 }

  def get_url_number = {

  }

  def delete_url = {

  }

  def get_content(url: String) = {//TODO url validation
    scala.io.Source.fromURL(url).mkString
  }



}